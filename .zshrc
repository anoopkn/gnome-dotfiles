# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi


# Environment Variables
export KITTY_ENABLE_WAYLAND=1
export ZYPP_MEDIANETWORK=1


# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=2000
SAVEHIST=20000
setopt autocd extendedglob notify
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/anoop/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
source ~/powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh


alias ls='lsd'
alias hdd='echo "\n\033[1m/dev/sda5\033[0m" > filesystem_info && sudo btrfs fi usage / >> filesystem_info && echo >> filesystem_info && (lsblk -o NAME,SIZE,FSAVAIL,FSUSE%,MOUNTPOINT) >> filesystem_info && sed -i '34d' filesystem_info && sed -e 's/FSAVAIL/AVAILABLE/g' -e 's/FSUSE%/USED/g' filesystem_info && rm filesystem_info'
alias history='history 1'
alias g++='g++ -pedantic-errors -Werror -std=c++17'
alias vim='nvim'
export PATH="$PATH:$HOME/.spicetify"








